# [Section] Comments
# single-line comment
"""
multi-line comment
"""

print('Hello World');

# [Section] Variable
age = 35; 
middle_initial = "C";

print(age);
print(middle_initial);

name1, name2, name3, name4 = "John", "Paul", "Ringo", "George";

# [Section] Data Types
# strings
full_name = "John Doe";
secret_code = 'Pa$$word';
print(full_name);
print(secret_code);


# numbers
number_of_days = 365 #int
pi_approx = 3.1415 #decimal/float
complex_num = 1 + 5j #j represents imaginary number
print(number_of_days);
print(pi_approx);
print(complex_num);

# numbers
isLearning = True; #capital letters for boolean values
isDifficult = False;

# [Section] Using Variables
print("My name is " + "'" + full_name + "'");
print("My age is " + str(age));
print(int("9387"));
print(float(3));

# [Section] Typecasing
print("My age is " + str(age));
bool_var = True;
print("value:", str(bool_var));
print(int(3.5));
print(int("98365"));
# print(int("swer"));
print(float(3));

# [Section] F-Strings
print(f"Hi! My name is {full_name}! My age is {age}!");

print(type(full_name));


# [Section] Operations
# Arithmetic
print(1 + 10);
print(1 - 10);
print(1 * 10);
print(1 / 10);
print(1 % 10);
print(1 ** 10);


# assignment operators
num1 = 3; 
print(num1);
num1 += 4;
print(num1);
# -=, +=, /=, %=

#comparison operators
print(1 == 1);
print(1 == "1");
# !=, >, <, >=, <=

# logical operators
print(True and False);
print(not False);
print(True or False);

